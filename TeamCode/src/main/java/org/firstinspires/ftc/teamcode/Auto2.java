package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.hardware.DcMotor;

import org.bobbob.AutoBot;

@Autonomous(name = "Auto_2", group = "FTC3")
public class Auto2 extends AutoBot {

    int pushGoldStep = 6;

    @Override
    public void modeLoop() {
        switch (step) {
            case 0:
                currentStep = "Go forward";
                if (drive.walk(-1000)) nextStep();
                break;
            case 1:
                currentStep = "Scan Center";
                drive.stop();
                if (findGold()) {
                    gold = 1;
                    nextStep();
                    step = pushGoldStep;
                } else if (mRuntime.time() > 1.5) {
                    nextStep();
                }
                break;
            case 2:
                currentStep = "Go to left";
                if (drive.slide(2000)) nextStep();
                break;
            case 3:
                currentStep = "Scan Left";
                if (findGold()) {
                    gold = 0;
                    nextStep();
                    step = pushGoldStep;
                } else if (mRuntime.time() > 1.5) {
                    nextStep();
                }
                break;
            case 4:
                currentStep = "Go to right";
                if (drive.slide(-4000)) nextStep();
                break;
            case 5:
                currentStep = "Scan Right";
                if (findGold()) {
                    gold = 2;
                    nextStep();
                    step = pushGoldStep;
                } else if (mRuntime.time() > 1.5) {
                    nextStep();
                }
                break;
            case 6:
                currentStep = "Push gold";
                if (drive.walk(-1500)) nextStep();
                break;
            case 7:
                currentStep = "Back";
                if (drive.walk(1000)) nextStep();
                break;
            case 8:
                currentStep = "Go to block point";
                int dist = -2800;
                if (gold == 1) dist -= 2000;
                else if (gold == 0) dist -= 4500;
                if (drive.slide(dist)) nextStep();
                break;
            case 9:
                currentStep = "turn";
                drive.setTurn(-135);
                drive.turn();
                if (drive.isTargetHeading()) {
                    drive.stop();
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    nextStep();
                }
                break;
            case 10:
                currentStep = "Wall";
                if (drive.slide(2500)) nextStep();
                break;
            case 11:
                currentStep = "Wall 2";
                if (drive.slide(-100)) nextStep();
                break;
            case 12:
                currentStep = "Go to cargo";
                if (drive.walk(500)) nextStep();
                break;
            case 13:
                currentStep = "extend";
                fw.setMode(DcMotor.RunMode.RUN_TO_POSITION);
                fw.setPower(0.5);
                fw.setTargetPosition(2000);

                if (Math.abs(fw.getCurrentPosition() - fw.getTargetPosition()) < 10) nextStep();
                break;
            case 14:
                currentStep = "down sb";
                sb.setTargetPosition(520);
                if(mRuntime.time() > 2) nextStep();
                break;
            default:
                sb.setPower(0);
                drive.stop();
                fw.setPower(0);
                break;
        }
    }
}

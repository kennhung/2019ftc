package org.firstinspires.ftc.teamcode;

import com.acmerobotics.dashboard.config.Config;
import com.qualcomm.robotcore.hardware.PIDCoefficients;

@Config
public class Const {
    public static PIDCoefficients TURNING_PID = new PIDCoefficients(0.01,0,0);
    public static int goldPos = 1;
}

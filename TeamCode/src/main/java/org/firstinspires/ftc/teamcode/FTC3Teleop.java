package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.bobbob.Robot;

@TeleOp(name = "ftc3_teleop", group = "FTC3")
public class FTC3Teleop extends Robot {

    private boolean llAuto, fwAuto, retreating, uped = false, uping = false;
    private ElapsedTime upTimer = new ElapsedTime();

    @Override
    public void initialization() {
        sb.setPower(0.6);
    }

    @Override
    public void start() {
        llAuto = false;
        fwAuto = false;
        retreating = false;
    }

    @Override
    public void loop() {
        drive.control(gamepad1);

        double fwSpeed = gamepad1.right_trigger - gamepad1.left_trigger;
        double llSpeed = gamepad2.right_trigger - gamepad2.left_trigger;

        if (llSpeed != 0) {
            llAuto = false;
        }

        if (fwSpeed != 0) {
            fwAuto = false;
        }

        if (llAuto) {
            ll.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            ll.setPower(0.5);
        } else {
            ll.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
            ll.setPower(llSpeed);
        }

        if (fwAuto) {
            fw.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            fw.setPower(0.5);
        } else {
            fw.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
            fw.setPower(fwSpeed);
        }

        if (gamepad2.b) {
            sb.setTargetPosition(200);
            fwAuto = true;
            fw.setTargetPosition(0);
            retreating = true;
        }

        if (gamepad2.back) {
            llAuto = true;
            ll.setTargetPosition(0);
        }

        if (retreating) {
            if (Math.abs(fw.getCurrentPosition() - fw.getTargetPosition()) < 50) {
                sb.setTargetPosition(-10);
            }

            if (Math.abs(sb.getCurrentPosition() - (-10)) < 5) {
                retreating = false;
            }
        }

        if (tServoPos >= 0 && tServoPos <= 0.7) {
            if (gamepad1.dpad_up) {
                tServoPos += 0.2;
            } else if (gamepad1.dpad_down) {
                tServoPos -= 0.2;
            }
        } else if (tServoPos < 0) {
            tServoPos = 0;
        } else if (tServoPos > 0.7) {
            tServoPos = 0.7;
        }

        t1.setPosition(tServoPos);
        t2.setPosition(tServoPos);

        if (gamepad2.right_bumper) {
            up.setPower(1);
            uping = false;
        } else if (gamepad2.left_bumper) {
            up.setPower(-1);
            uping = false;
        } else if(!uping) {
            up.setPower(0);
        }

        if (gamepad2.y) {
            jj.setPosition(0);
        } else if (gamepad2.a) {
            jj.setPosition(1);
        } else {
            jj.setPosition(0.5);
        }

        if (gamepad2.dpad_up && sb.getCurrentPosition() >= -30) {
            sb.setTargetPosition(sb.getCurrentPosition() - 60);
        } else if (gamepad2.dpad_down && sb.getCurrentPosition() <= 520) {
            sb.setTargetPosition(sb.getCurrentPosition() + 60);
        }

        if (gamepad2.dpad_down || gamepad2.dpad_up) {
            retreating = false;
        }

        if (sb.getCurrentPosition() > 520) sb.setTargetPosition(520);
        else if (sb.getCurrentPosition() < -30) sb.setTargetPosition(-30);

        if(gamepad2.right_stick_button && !uped){
            uped = true;
            uping = true;
            upTimer.reset();
            up.setPower(-1);
        }

        if ((uping && upTimer.time() > 9 )){
            uping = false;
            up.setPower(0);
        }

        dashboard();
    }

    private void dashboard() {
        packet.put("tServoPos", tServoPos);
        telemetry.addData("tServoPos", tServoPos);

        packet.put("heading", gyro.getHeading());
        telemetry.addData("heading", gyro.getHeading());

        telemetry.addData("sb enc pos", sb.getCurrentPosition());
        telemetry.addData("ll enc pos", ll.getCurrentPosition());
        telemetry.addData("fw enc pos", fw.getCurrentPosition());

        packet.fieldOverlay().setStroke("blue").strokeRect(-20, -20, 10, 10);

        dashboard.sendTelemetryPacket(packet);
        telemetry.update();
    }
}

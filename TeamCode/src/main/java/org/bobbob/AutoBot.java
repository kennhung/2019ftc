package org.bobbob;

import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.robotcore.external.ClassFactory;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer;
import org.firstinspires.ftc.robotcore.external.tfod.Recognition;
import org.firstinspires.ftc.robotcore.external.tfod.TFObjectDetector;

import java.util.List;

public abstract class AutoBot extends Robot {
    protected static final String TFOD_MODEL_ASSET = "RoverRuckus.tflite";
    protected static final String LABEL_GOLD_MINERAL = "Gold Mineral";
    protected static final String LABEL_SILVER_MINERAL = "Silver Mineral";

    String VUFORIA_KEY = "AQxzpRz/////AAAAGYkkpQ+q/Eiimg4q4LgBavU9UqqpGZsuQiDXExq4Ekl5TlbHlTxkp3svnn1+1D2dAatUibcPlia/EC977mNvUtlMFcCtPgKhHZ025dzhWGxqYs0hlM4+grISkdh3EnalvwAYtknm2yyacvnX2quKLvWpD/KjTceVAFqvMDn0YFJhL8x3Q/H/QY2wbFjJY4Kv6VYQ3JpLQYGW5bSKaLomXZ3l2a1aka62AxQ8BnWLdamNE8uJFc9s+9JewRmq1Sc4cvMGrjl9L8N4cfnxv3qN0jlIKpTPtkG/W+cUWFbSiJZZnM4SZAwUJAoMjd3WjjQ3ZObq8DJyMVfQtttDogXn3vEKToX02ssgcZhwltZvIBU9";

    protected VuforiaLocalizer vuforia;
    protected TFObjectDetector tfod;

    protected int step = 0;
    protected int gold = -1;
    protected String currentStep = "none";

    protected ElapsedTime mRuntime = new ElapsedTime();

    @Override
    public void initialization() {
        initVuforia();

        if (ClassFactory.getInstance().canCreateTFObjectDetector()) {
            initTfod();
        } else {
            telemetry.addData("Sorry!", "This device is not compatible with TFOD");
        }

        jhj.setPosition(0);
        jj.setPosition(0.5);

        /** Wait for the game to begin */
        currentStep = "Press Play to start tracking";
        telemetry.addData(">", currentStep);
        telemetry.update();
    }

    @Override
    public void start() {
        step = 0;
        gold = -1;
        if (tfod != null) {
            tfod.activate();
        }
        mRuntime.reset();

        sb.setPower(0.3);
        sb.setTargetPosition(5);
    }

    public abstract void modeLoop();

    @Override
    public void loop() {
        modeLoop();

        telemetry.addData(">", currentStep);
        telemetry.addData("Gold Mineral Position", (gold == 0 ? "Left" : (gold == 1 ? "Center" : (gold == 2 ? "Right" : "not found"))));

        packet.put("heading", gyro.getHeading());
        telemetry.addData("heading", gyro.getHeading());
        packet.put("errorHeading", drive.getErrorHeading());
        telemetry.addData("dist", drive.getBRPos());
        telemetry.addData("sb pos", sb.getCurrentPosition());

        telemetry.update();
        dashboard.sendTelemetryPacket(packet);
    }

    protected boolean findGold() {
        List<Recognition> updatedRecognitions = tfod.getUpdatedRecognitions();
        if (updatedRecognitions != null) {
            telemetry.addData("# Object Detected", updatedRecognitions.size());
            if (updatedRecognitions.size() == 1) {
                for (Recognition recognition : updatedRecognitions) {
                    if (recognition.getLabel().equals(LABEL_GOLD_MINERAL)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    protected void nextStep() {
        drive.stop();
        step++;
        drive.reset();
        mRuntime.reset();
    }

    private void initVuforia() {
        /*
         * Configure Vuforia by creating a Parameter object, and passing it to the Vuforia engine.
         */
        VuforiaLocalizer.Parameters parameters = new VuforiaLocalizer.Parameters();

        parameters.vuforiaLicenseKey = VUFORIA_KEY;
        parameters.cameraDirection = VuforiaLocalizer.CameraDirection.BACK;

        //  Instantiate the Vuforia engine
        vuforia = ClassFactory.getInstance().createVuforia(parameters);

        // Loading trackables is not necessary for the Tensor Flow Object Detection engine.
    }


    private void initTfod() {
        int tfodMonitorViewId = hardwareMap.appContext.getResources().getIdentifier(
                "tfodMonitorViewId", "id", hardwareMap.appContext.getPackageName());
        TFObjectDetector.Parameters tfodParameters = new TFObjectDetector.Parameters(tfodMonitorViewId);
        tfodParameters.minimumConfidence = 0.3;
        tfodParameters.useObjectTracker = true;
        tfod = ClassFactory.getInstance().createTFObjectDetector(tfodParameters, vuforia);
        tfod.loadModelFromAsset(TFOD_MODEL_ASSET, LABEL_GOLD_MINERAL, LABEL_SILVER_MINERAL);
    }
}

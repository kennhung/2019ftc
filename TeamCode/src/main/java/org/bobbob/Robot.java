package org.bobbob;

import com.acmerobotics.dashboard.FtcDashboard;
import com.acmerobotics.dashboard.telemetry.TelemetryPacket;
import com.qualcomm.hardware.modernrobotics.ModernRoboticsI2cGyro;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.Servo;

public abstract class Robot extends OpMode {

    protected DcMotor m_fl, m_fr, m_bl, m_br;
    protected DcMotor up;
    protected DcMotor ll, fw, sb;

    protected Servo t1, t2, jj, jhj;
    protected double tServoPos = 0;
    protected ModernRoboticsI2cGyro gyro;

    protected FtcDashboard dashboard;
    protected TelemetryPacket packet;

    protected MecanumDrive drive;

    @Override
    public void init() {
        dashboard = FtcDashboard.getInstance();
        packet = new TelemetryPacket();

        m_fl = hardwareMap.get(DcMotor.class, "FL");
        m_fr = hardwareMap.get(DcMotor.class, "FR");
        m_bl = hardwareMap.get(DcMotor.class, "BL");
        m_br = hardwareMap.get(DcMotor.class, "BR");


        up = hardwareMap.get(DcMotor.class, "UP");
        ll = hardwareMap.get(DcMotor.class, "LL");
        fw = hardwareMap.get(DcMotor.class, "FW");
        sb = hardwareMap.get(DcMotor.class, "SB");

        t1 = hardwareMap.get(Servo.class, "T1");
        t2 = hardwareMap.get(Servo.class, "T2");
        jj = hardwareMap.get(Servo.class, "JJ");
        jhj = hardwareMap.get(Servo.class, "JHJ");

        gyro = hardwareMap.get(ModernRoboticsI2cGyro.class, "gyro");

        t2.setDirection(Servo.Direction.REVERSE);

        m_fl.setDirection(DcMotorSimple.Direction.REVERSE);
        m_bl.setDirection(DcMotorSimple.Direction.REVERSE);
        sb.setMode(DcMotor.RunMode.RUN_TO_POSITION);

        drive = new MecanumDrive(m_fl, m_fr, m_bl, m_br, gyro);

        dashboard.sendTelemetryPacket(packet);

        initialization();
    }

    public abstract void initialization();
}

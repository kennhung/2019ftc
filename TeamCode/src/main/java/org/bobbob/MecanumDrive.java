package org.bobbob;

import com.qualcomm.hardware.modernrobotics.ModernRoboticsI2cGyro;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Gamepad;

import org.firstinspires.ftc.teamcode.Const;

public class MecanumDrive {
    private DcMotor fl, fr, bl, br;
    private ModernRoboticsI2cGyro gyro;
    private int flPos = 0, frPos = 0, blPos = 0, brPos = 0;

    private double baseSpeed = 0.7;

    private int targetHeading = 0;

    public MecanumDrive(DcMotor fl, DcMotor fr, DcMotor bl, DcMotor br, ModernRoboticsI2cGyro gyro) {
        this.bl = bl;
        this.br = br;
        this.fl = fl;
        this.fr = fr;

        this.gyro = gyro;
    }

    public void control(Gamepad gamepad) {
        double driveSpeed1 = gamepad.left_stick_x + gamepad.left_stick_y;
        double driveSpeed2 = gamepad.left_stick_y - gamepad.left_stick_x;

        fl.setPower(driveSpeed1 + gamepad.right_stick_x);
        bl.setPower(driveSpeed2 + gamepad.right_stick_x);

        fr.setPower(driveSpeed2 - gamepad.right_stick_x);
        br.setPower(driveSpeed1 - gamepad.right_stick_x);
    }

    public boolean walk(int dis) {
        if (isTargetReached(dis)) {
            stop();
            return true;
        }

        int direction = 1;

        if (dis < 0) {
            direction = -1;
        }

        double driveSpeed1 = -baseSpeed * direction;
        double driveSpeed2 = -baseSpeed * direction;

        double editPower = getErrorHeading() * Const.TURNING_PID.p;

        fl.setPower(driveSpeed1 + editPower);
        bl.setPower(driveSpeed2 + editPower);

        fr.setPower(driveSpeed2 - editPower);
        br.setPower(driveSpeed1 - editPower);
        return false;
    }

    public boolean slide(int dis) {
        if (isTargetReached(dis)) {
            stop();
            return true;
        }

        int direction = 1;

        if (dis < 0) {
            direction = -1;
        }

        double driveSpeed1 = baseSpeed * direction;
        double driveSpeed2 = -baseSpeed * direction;

        double editPower = getErrorHeading() * Const.TURNING_PID.p;

        fl.setPower(driveSpeed1 + editPower);
        bl.setPower(driveSpeed2 + editPower);

        fr.setPower(driveSpeed2 - editPower);
        br.setPower(driveSpeed1 - editPower);
        return false;
    }

    public void setTurn(int heading){
        targetHeading = heading;
    }

    public void turn() {
        double editPower = getErrorHeading() * Const.TURNING_PID.p;

        fl.setPower(editPower);
        bl.setPower(editPower);

        fr.setPower(-editPower);
        br.setPower(-editPower);
    }

    public int getCurrentHeading() {
        int currentHeading = gyro.getHeading();
        if (currentHeading > 180) {
            currentHeading = currentHeading - 360;
        }
        return currentHeading;
    }

    public int getErrorHeading() {
        int current = getCurrentHeading();

        if (Math.abs(targetHeading) > 160) {
            current = gyro.getHeading();
        }
        return current - targetHeading;
    }

    public boolean isTargetReached(int dis) {
        return Math.abs(getBRPos()) > Math.abs(dis);
    }

    public boolean isTargetHeading() {
        return Math.abs(getErrorHeading()) < 10;
    }

    public void stop() {
        fl.setPower(0);
        bl.setPower(0);
        fr.setPower(0);
        br.setPower(0);
    }

    public int getFLPos() {
        return fl.getCurrentPosition() - flPos;
    }

    public int getFRPos() {
        return fr.getCurrentPosition() - frPos;
    }

    public int getBLPos() {
        return bl.getCurrentPosition() - blPos;
    }

    public int getBRPos() {
        return br.getCurrentPosition() - brPos;
    }

    public void reset() {
        flPos = fl.getCurrentPosition();
        frPos = fr.getCurrentPosition();
        blPos = bl.getCurrentPosition();
        brPos = br.getCurrentPosition();

    }
}
